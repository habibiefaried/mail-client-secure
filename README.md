# email-client-secure
Email-client-secure merupakan sebuah klien surel yang mengimplementasikan jaringan email yang aman: (1) enkripsi dengan block cipher dan (2) tanda tangan digital dengan SHA-1 dan ECDSA (Elliptic Curve Digital Signature Algorithm).  
  
Petunjuk menjalankan program:
1. Buatlah sebuah project java dalam Eclipse  
2. Copy-paste semua file/folder dalam folder src ke dalam project  
3. Jalankan program dari kelas Main.java dalam package default  
  
oleh:  
Muhammad Nassirudin (13511044) dan Habibie Faried (13511069)
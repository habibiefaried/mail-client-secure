import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

import util.ECDSAImplementation;
import util.ECDSAPoint;
import util.EllipticCurve;
import util.Email;
import util.EmailApp;
import util.EmailTableModel;
import util.MessageTranslator;

/**
 * main class and GUI
 */

/**
 * @author nashir
 *
 */
public class Main {

	private static Main instance;
	private static Toolkit toolkit;
	private static Dimension screenSize;
	private static Insets inset;
	
	private static JFrame mainFrame;
	private static JFrame keyFrame;

	private static GroupLayout mainLayout;
	
	private static JPanel menuPanel;
	private static JPanel sidePanel;
	private static JPanel composePanel;
	private static JPanel inboxPanel;
	private static JPanel outboxPanel;
	
	private static JButton generatorButton;
	private static JLabel privateKeyLabel;
	private static JTextField privateKeyField;
	private static JLabel publicKeyLabel;
	private static JTextField publicKeyField;
	private static JButton cancelButton;
	private static JButton setButton;
	
	private static JButton keyButton;
	private static JButton replyButton;
	private static JButton forwardButton;
	private static JToggleButton encryptionToggle;
	private static JToggleButton signatureToggle;
	
	private static JButton composeButton;
	private static JButton inboxButton;
	private static JButton outboxButton;
	
	private static JLabel toLabel;
	private static JTextField toField;
	private static JLabel subjectLabel;
	private static JTextField subjectField;
	private static JTextArea editorArea;
	private static JScrollPane editorScroll;
	private static JButton attachmentButton;
	private static JButton sendButton;
	
	private static JLabel inboxCountLabel;
	private static JTable inboxTable;
	private static JScrollPane inboxScroll;
	private static JTextArea inboxArea;
	private static JScrollPane inboxViewScroll;
	private static JPanel inboxAttachmentPanel;
	private static GroupLayout inboxAttachmentPanelLayout;
	private static JButton[] inboxAttachmentButtons;
	
	private static JLabel outboxCountLabel;
	private static JTable outboxTable;
	private static JScrollPane outboxScroll;
	private static JTextArea outboxArea;
	private static JScrollPane outboxViewScroll;
	private static JPanel outboxAttachmentPanel;
	private static GroupLayout outboxAttachmentPanelLayout;
	private static JButton[] outboxAttachmentButtons;
	
	private static EllipticCurve curve;
	private static String privateKey;
	private static String publicKey;

	private static BigInteger e_content;
	private static ECDSAPoint e_hash;
	
	private static Email[] inboxMails;
	private static Email[] outboxMails;
	
	public static Main getInstance() {
		if (instance == null) {
			instance = new Main();
			
			//JOptionPane.showMessageDialog(null, "Welcome to mail-client-secure!");
			
			System.out.println("Preparing data...");
			prepareData();
			
			System.out.println("Preparing GUI...");
			prepareGUI();
		}
		return instance;
	}
	
	private static void prepareGUI() {
		// main frame and screen's information
		toolkit = Toolkit.getDefaultToolkit();
		mainFrame = new JFrame("myEmail");
		screenSize = toolkit.getScreenSize();
		inset = toolkit.getScreenInsets(mainFrame.getGraphicsConfiguration());
		mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		mainFrame.setSize(screenSize.width - inset.left - inset.right, screenSize.height - inset.top - inset.bottom);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// main layout
		mainLayout = new GroupLayout(mainFrame.getContentPane());
		mainLayout.setAutoCreateGaps(true);
		mainLayout.setAutoCreateContainerGaps(true);
		mainFrame.getContentPane().setLayout(mainLayout);
		
		// key frame
		keyFrame = new JFrame("Keys Generator");
		keyFrame.setResizable(false);
		keyFrame.setSize(400, 160);
		keyFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// key layout
		GroupLayout keyLayout = new GroupLayout(keyFrame.getContentPane());
		keyLayout.setAutoCreateGaps(true);
		keyLayout.setAutoCreateContainerGaps(true);
		keyFrame.getContentPane().setLayout(keyLayout);
		
		// keys generator's objects
		generatorButton = new JButton("Generate new");
		generatorButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// generate private key
				long private_key = new Random().nextLong() % curve.getP();
				while (private_key < 0) private_key += curve.getP();
				if (private_key == 0L) private_key = curve.getP()/2;
				privateKeyField.setText(Long.toString(private_key));
				
				// generate public key
				long[] public_key = curve.getPublicKey(private_key);
				publicKeyField.setText("(" + Long.toString(public_key[0]) + "," + Long.toString(public_key[1]) + ")");
			}
		});
		
		privateKeyLabel = new JLabel("Private key");
		privateKeyField = new JTextField();
		privateKeyField.setEditable(false);
		
		publicKeyLabel = new JLabel("Public key");
		publicKeyField = new JTextField();
		publicKeyField.setEditable(false);
		
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				keyFrame.dispose();
			}
		});
		setButton = new JButton("Set");
		setButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				privateKey = privateKeyField.getText();
				publicKey = publicKeyField.getText();
				
				PrintWriter pw = null;
				try {
					pw = new PrintWriter("res/key/key.pri");
					pw.write(privateKey);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					if (pw != null) pw.close();
				}
				try {
					pw = new PrintWriter("res/key/key.pub");
					pw.write(publicKey);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					if (pw != null) pw.close();
				}
				
				keyFrame.dispose();
			}
		});
		
		// key frame's layout
		keyLayout.setHorizontalGroup(keyLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(generatorButton, keyFrame.getWidth()/2, keyFrame.getWidth(), keyFrame.getWidth())
			.addGroup(keyLayout.createSequentialGroup()
				.addComponent(privateKeyLabel, 75, 75, 75)
				.addComponent(privateKeyField))
			.addGroup(keyLayout.createSequentialGroup()
				.addComponent(publicKeyLabel, 75, 75, 75)
				.addComponent(publicKeyField))
			.addGroup(keyLayout.createSequentialGroup()
				.addComponent(cancelButton, keyFrame.getWidth()/3, keyFrame.getWidth()/2, keyFrame.getWidth()/2)
				.addComponent(setButton, keyFrame.getWidth()/3, keyFrame.getWidth()/2, keyFrame.getWidth()/2)));
		keyLayout.setVerticalGroup(keyLayout.createSequentialGroup()
			.addComponent(generatorButton)
			.addGroup(keyLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(privateKeyLabel)
				.addComponent(privateKeyField))
			.addGroup(keyLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(publicKeyLabel)
				.addComponent(publicKeyField))
			.addGroup(keyLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(cancelButton)
				.addComponent(setButton)));
		
		// menu panel
		menuPanel = new JPanel();
		menuPanel.setBorder(BorderFactory.createTitledBorder("Menu Bar"));
		
		// menuPanel: layout
		GroupLayout menuLayout = new GroupLayout(menuPanel);
		menuLayout.setAutoCreateGaps(true);
		menuLayout.setAutoCreateContainerGaps(true);
		menuPanel.setLayout(menuLayout);
		
		// menuPanel: key generator button
		keyButton = new JButton("Generate Keys");
		keyButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				privateKeyField.setText(privateKey);
				publicKeyField.setText(publicKey);
				keyFrame.setVisible(true);
			}
		});
		
		// menuPanel: reply button
		replyButton = new JButton("Reply");
		replyButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// menuPanel: forward button
		forwardButton = new JButton("Forward");
		forwardButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// menuPanel: toggle button for encryption
		encryptionToggle = new JToggleButton("Encryption");
		
		// menuPanel: toggle button for signature
		signatureToggle = new JToggleButton("Signature");
		
		// menuPanel: panel's layout
		menuLayout.setHorizontalGroup(menuLayout.createSequentialGroup()
			.addComponent(keyButton)
			.addGap(32)
			.addComponent(replyButton)
			.addComponent(forwardButton)
			.addGap(32)
			.addComponent(encryptionToggle)
			.addComponent(signatureToggle));
		menuLayout.setVerticalGroup(menuLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			.addComponent(keyButton)
			.addComponent(replyButton)
			.addComponent(forwardButton)
			.addComponent(encryptionToggle)
			.addComponent(signatureToggle));
		
		// side panel
		sidePanel = new JPanel();
		sidePanel.setBorder(BorderFactory.createTitledBorder("Side Bar"));
		
		// sidePanel: layout
		GroupLayout sideLayout = new GroupLayout(sidePanel);
		sideLayout.setAutoCreateGaps(true);
		sideLayout.setAutoCreateContainerGaps(true);
		sidePanel.setLayout(sideLayout);
		
		// sidePanel: compose button
		composeButton = new JButton("Compose");
		composeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.add(composePanel);
				mainFrame.remove(inboxPanel);
				mainFrame.remove(outboxPanel);
				
				// update main frame's layout
				mainLayout.setHorizontalGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(menuPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
					.addGroup(mainLayout.createSequentialGroup()
						.addComponent(sidePanel)
						.addComponent(composePanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())));
				mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
					.addComponent(menuPanel)
					.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(sidePanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())
						.addComponent(composePanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())));
				
				mainFrame.repaint();
			}
		});
		
		// sidePanel: inbox button
		inboxButton = new JButton("Inbox");
		inboxButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.remove(composePanel);
				mainFrame.add(inboxPanel);
				mainFrame.remove(outboxPanel);
				
				// update main frame's layout
				mainLayout.setHorizontalGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(menuPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
					.addGroup(mainLayout.createSequentialGroup()
						.addComponent(sidePanel)
						.addComponent(inboxPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())));
				mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
					.addComponent(menuPanel)
					.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(sidePanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())
						.addComponent(inboxPanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())));
				
				mainFrame.repaint();
			}
		});
		
		// sidePanel: outbox button
		outboxButton = new JButton("Outbox");
		outboxButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.remove(composePanel);
				mainFrame.remove(inboxPanel);
				mainFrame.add(outboxPanel);
				
				// update main frame's layout
				mainLayout.setHorizontalGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(menuPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
					.addGroup(mainLayout.createSequentialGroup()
						.addComponent(sidePanel)
						.addComponent(outboxPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())));
				mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
					.addComponent(menuPanel)
					.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(sidePanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())
						.addComponent(outboxPanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())));
				
				mainFrame.repaint();
			}
		});
		
		// sidePanel: panel's layout
		sideLayout.setHorizontalGroup(sideLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(composeButton, 120, 120, 120)
			.addComponent(inboxButton, 120, 120, 120)
			.addComponent(outboxButton, 120, 120, 120));
		sideLayout.setVerticalGroup(sideLayout.createSequentialGroup()
			.addComponent(composeButton)
			.addComponent(inboxButton)
			.addComponent(outboxButton));
		
		// compose panel
		composePanel = new JPanel();
		composePanel.setBorder(BorderFactory.createTitledBorder("Editor"));
		
		// composePanel: layout
		GroupLayout composeLayout = new GroupLayout(composePanel);
		composeLayout.setAutoCreateGaps(true);
		composeLayout.setAutoCreateContainerGaps(true);
		composePanel.setLayout(composeLayout);
		
		// composePanel: recipient's email address
		toLabel = new JLabel("To");
		toField = new JTextField();
		
		// composePanel: email's subject
		subjectLabel = new JLabel("Subject");
		subjectField = new JTextField();
		
		// composePanel: editor
		editorArea = new JTextArea();
		editorScroll = new JScrollPane(editorArea);
		
		// composePanel: attachment button
		attachmentButton = new JButton("Add attachment");
		attachmentButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// composePanel: send button
		sendButton = new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String message = editorArea.getText();
				System.out.println(message);
				
				if (toField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Recipient is empty.");
					return;
				}
				
				if (subjectField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Recipient is empty.");
					return;
				}
				
				if (signatureToggle.isSelected()) {
					try {
						BigInteger hash = new BigInteger(ECDSAImplementation.SHA1(message)); // misalkan ini hashplain
						ECDSAPoint EP = ECDSAImplementation.writeSign(hash); // pengambilan dsa
						//EP.printHex();
						//System.out.println("Signature: " + EP.toString());
						
						// append DS at the end of message
						message += "\n\n\n*** Begin of digital signature ****\n";
						message += EP.toString();
						message += "\n*** End of digital signature ****\n";
						
						//System.out.println(ECDSAImplementation.verifySign(EP, new BigInteger(ECDSAImplementation.SHA1(message))));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
				
				String key = null;
				if (encryptionToggle.isSelected()) {
					key = JOptionPane.showInputDialog(null, "Input encryption's key:");
					if (key != null) {
						MessageTranslator.setMode1(MessageTranslator.MODE_ENCRYPTION);
						MessageTranslator.setMode2(MessageTranslator.MODE_ECB);
						MessageTranslator.prepareEncryptKeys(key);
						byte[] result = MessageTranslator.translateMessage(message.getBytes());
						message = new String(result);
					}
				}
				
				editorArea.setText(message);
				EmailApp.sendMail(subjectField.getText(), message, toField.getText());
				updateEmail();
				
				inboxCountLabel.setText(inboxMails.length + " email(s).");
				outboxCountLabel.setText(outboxMails.length + " email(s).");
				
				inboxTable.setModel(new EmailTableModel(inboxMails));
				outboxTable.setModel(new EmailTableModel(outboxMails));
			}
		});
		
		// composePanel: panel's layout
		composeLayout.setHorizontalGroup(composeLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(composeLayout.createSequentialGroup()
				.addComponent(toLabel, 75, 75, 75)
				.addComponent(toField))
			.addGroup(composeLayout.createSequentialGroup()
				.addComponent(subjectLabel, 75, 75, 75)
				.addComponent(subjectField))
			.addComponent(editorScroll)
			.addGroup(composeLayout.createSequentialGroup()
				.addComponent(sendButton)
				.addComponent(attachmentButton)));
		composeLayout.setVerticalGroup(composeLayout.createSequentialGroup()
			.addGroup(composeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(toLabel)
				.addComponent(toField))
			.addGroup(composeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(subjectLabel)
				.addComponent(subjectField))
			.addComponent(editorScroll)
			.addGroup(composeLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(sendButton)
				.addComponent(attachmentButton)));
		
		// inbox panel
		inboxPanel = new JPanel();
		inboxPanel.setBorder(BorderFactory.createTitledBorder("Received Emails"));
		
		// inboxPanel: layout
		GroupLayout inboxLayout = new GroupLayout(inboxPanel);
		inboxLayout.setAutoCreateGaps(true);
		inboxLayout.setAutoCreateContainerGaps(true);
		inboxPanel.setLayout(inboxLayout);
		
		// inboxPanel: count label
		inboxCountLabel = new JLabel(inboxMails.length + " email(s).");
		
		// inboxPanel: list of email
		EmailTableModel inboxTableModel = new EmailTableModel(inboxMails);
		inboxTable = new JTable(inboxTableModel);
		inboxScroll = new JScrollPane(inboxTable);
		inboxTable.setFillsViewportHeight(true);
		TableColumn column = null;
		for (int i = 0; i < inboxTableModel.getColumnCount(); i++) {
			column = inboxTable.getColumnModel().getColumn(i);
			if (i == 0) column.setPreferredWidth(mainFrame.getWidth()/4);
			if (i == 1) column.setPreferredWidth(mainFrame.getWidth()/2);
		}
		inboxTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (inboxTable.getSelectedRow() > -1) {
					Email mail = inboxMails[inboxTable.getSelectedRow()];
					String message = mail.getContent();
					
					String key = null;
					if (encryptionToggle.isSelected()) {
						key = JOptionPane.showInputDialog("Input decryption's key:");
						if (key != null) {
							MessageTranslator.setMode1(MessageTranslator.MODE_DECRYPTION);
							MessageTranslator.setMode2(MessageTranslator.MODE_ECB);
							MessageTranslator.prepareDecryptKeys(key);
							byte[] result = MessageTranslator.translateMessage(message.getBytes());
							message = new String(result);
						} else {
							return;
						}
					}
					
					if (signatureToggle.isSelected()) {
						int i = 0;
						
						// find actual message's content
						String content = "";
						while (!message.substring(i, i+35).equals("*** Begin of digital signature ****")) {
							content += message.charAt(i);
							i++;
						}
						content = content.substring(0, content.length()-6);
						e_content = new BigInteger(ECDSAImplementation.SHA1(content));
						//ECDSAPoint EP = ECDSAImplementation.writeSign(e_content); // pengambilan dsa
						//System.out.println("Signature: " + EP.toString());
						
						// find DS key
						i += 37;
						key = "";
						while (message.charAt(i) != '\n') {
							key += message.charAt(i);
							i++;
						}
						key = key.substring(0, key.length()-1);
						String[] parts = key.split("-");
						e_hash = new ECDSAPoint(parts[0], parts[1]);
						
						JOptionPane.showMessageDialog(null, "This email is " + ECDSAImplementation.verifySign(e_hash, e_content));
					}
					
					mail.setContent(message);
					inboxArea.setText(mail.toString());

					final String[] attachmentNames = mail.getAttachments();
					inboxAttachmentButtons = new JButton[attachmentNames.length];
					inboxAttachmentPanel.removeAll();
					for (int i = 0; i < attachmentNames.length; i++) {
						inboxAttachmentButtons[i] = new JButton(attachmentNames[i]);
						inboxAttachmentButtons[i].addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								try {
									int idx = 0;
									while (arg0.getSource() != inboxAttachmentButtons[idx]) idx++;
									File f = new File("res/inbox/attachments/" + attachmentNames[idx]);
									Desktop.getDesktop().open(f);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						inboxAttachmentPanel.add(inboxAttachmentButtons[i]);
					}
					
					SequentialGroup glh = inboxAttachmentPanelLayout.createSequentialGroup();
					ParallelGroup glv = inboxAttachmentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE);
					for (int i = 0; i < inboxAttachmentButtons.length; i++) {
						glh = glh.addComponent(inboxAttachmentButtons[i]);
						glv = glv.addComponent(inboxAttachmentButtons[i]);
					}
					inboxAttachmentPanelLayout.setHorizontalGroup(glh);
					inboxAttachmentPanelLayout.setVerticalGroup(glv);
				}
			}
		});
		
		// inboxPanel: email's view port
		inboxArea = new JTextArea();
		inboxArea.setLineWrap(true);
		inboxArea.setEditable(false);
		inboxViewScroll = new JScrollPane(inboxArea);
		
		// inboxPanel: attachment's port
		inboxAttachmentPanel = new JPanel();
		inboxAttachmentPanel.setBorder(BorderFactory.createTitledBorder("Attachment(s)"));
		inboxAttachmentPanelLayout = new GroupLayout(inboxAttachmentPanel);
		inboxAttachmentPanelLayout.setAutoCreateGaps(true);
		inboxAttachmentPanelLayout.setAutoCreateContainerGaps(true);
		inboxAttachmentPanel.setLayout(inboxAttachmentPanelLayout);
		
		// inboxPanel: panel's layout
		inboxLayout.setHorizontalGroup(inboxLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(inboxCountLabel)
			.addComponent(inboxScroll, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
			.addComponent(inboxViewScroll, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
			.addComponent(inboxAttachmentPanel));
		inboxLayout.setVerticalGroup(inboxLayout.createSequentialGroup()
			.addComponent(inboxCountLabel)
			.addComponent(inboxScroll)
			.addComponent(inboxViewScroll, mainFrame.getHeight()/3, mainFrame.getHeight()/3, mainFrame.getHeight())
			.addComponent(inboxAttachmentPanel));
		
		// outbox panel
		outboxPanel = new JPanel();
		outboxPanel.setBorder(BorderFactory.createTitledBorder("Sent Emails"));
		
		// outboxPanel: layout
		GroupLayout outboxLayout = new GroupLayout(outboxPanel);
		outboxLayout.setAutoCreateGaps(true);
		outboxLayout.setAutoCreateContainerGaps(true);
		outboxPanel.setLayout(outboxLayout);
		
		// outboxPanel: count label
		outboxCountLabel = new JLabel(outboxMails.length + " email(s).");
		
		// outboxPanel: list of email
		EmailTableModel outboxTableModel = new EmailTableModel(outboxMails);
		outboxTable = new JTable(outboxTableModel);
		outboxScroll = new JScrollPane(outboxTable);
		outboxTable.setFillsViewportHeight(true);
		column = null;
		for (int i = 0; i < outboxTableModel.getColumnCount(); i++) {
			column = outboxTable.getColumnModel().getColumn(i);
			if (i == 0) column.setPreferredWidth(mainFrame.getWidth()/4);
			if (i == 1) column.setPreferredWidth(mainFrame.getWidth()/2);
		}
		outboxTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (outboxTable.getSelectedRow() > -1) {
					Email mail = outboxMails[outboxTable.getSelectedRow()];
					outboxArea.setText(mail.toString());
					
					final String[] attachmentNames = mail.getAttachments();
					outboxAttachmentButtons = new JButton[attachmentNames.length];
					outboxAttachmentPanel.removeAll();
					for (int i = 0; i < attachmentNames.length; i++) {
						outboxAttachmentButtons[i] = new JButton(attachmentNames[i]);
						outboxAttachmentButtons[i].addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent arg0) {
								try {
									int idx = 0;
									while (arg0.getSource() != outboxAttachmentButtons[idx]) idx++;
									File f = new File("res/outbox/attachments/" + attachmentNames[idx]);
									Desktop.getDesktop().open(f);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						outboxAttachmentPanel.add(outboxAttachmentButtons[i]);
					}
					
					SequentialGroup glh = outboxAttachmentPanelLayout.createSequentialGroup();
					ParallelGroup glv = outboxAttachmentPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE);
					for (int i = 0; i < outboxAttachmentButtons.length; i++) {
						glh = glh.addComponent(outboxAttachmentButtons[i]);
						glv = glv.addComponent(outboxAttachmentButtons[i]);
					}
					outboxAttachmentPanelLayout.setHorizontalGroup(glh);
					outboxAttachmentPanelLayout.setVerticalGroup(glv);
				}
			}
		});
		
		// outboxPanel: email's view port
		outboxArea = new JTextArea();
		outboxArea.setLineWrap(true);
		outboxArea.setEditable(false);
		outboxViewScroll = new JScrollPane(outboxArea);
		
		// outboxPanel: attachment's port
		outboxAttachmentPanel = new JPanel();
		outboxAttachmentPanel.setBorder(BorderFactory.createTitledBorder("Attachment(s)"));
		outboxAttachmentPanelLayout = new GroupLayout(outboxAttachmentPanel);
		outboxAttachmentPanelLayout.setAutoCreateGaps(true);
		outboxAttachmentPanelLayout.setAutoCreateContainerGaps(true);
		outboxAttachmentPanel.setLayout(outboxAttachmentPanelLayout);
		
		// outboxPanel: panel's layout
		outboxLayout.setHorizontalGroup(outboxLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(outboxCountLabel)
			.addComponent(outboxScroll, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
			.addComponent(outboxViewScroll, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
			.addComponent(outboxAttachmentPanel));
		outboxLayout.setVerticalGroup(outboxLayout.createSequentialGroup()
			.addComponent(outboxCountLabel)
			.addComponent(outboxScroll)
			.addComponent(outboxViewScroll, mainFrame.getHeight()/3, mainFrame.getHeight()/3, mainFrame.getHeight())
			.addComponent(outboxAttachmentPanel));
		
		// main frame's layout
		mainLayout.setHorizontalGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addComponent(menuPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())
			.addGroup(mainLayout.createSequentialGroup()
				.addComponent(sidePanel)
				.addComponent(inboxPanel, mainFrame.getWidth()/2, mainFrame.getWidth(), mainFrame.getWidth())));
		mainLayout.setVerticalGroup(mainLayout.createSequentialGroup()
			.addComponent(menuPanel)
			.addGroup(mainLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(sidePanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())
				.addComponent(inboxPanel, mainFrame.getHeight()/2, mainFrame.getHeight(), mainFrame.getHeight())));
		
		// post-process
		mainFrame.add(menuPanel);
		mainFrame.add(sidePanel);
		mainFrame.add(inboxPanel);
		mainFrame.setVisible(true);
	}
	
	private static void updateEmail() {
		// load inbox emails
		inboxMails = EmailApp.checkMail(EmailApp.INBOX);
		
		// load outbox emails
		outboxMails = EmailApp.checkMail(EmailApp.OUTBOX);
	}
	
	private static void prepareData() {
		// load elliptic curve's default parameters
		curve = new EllipticCurve();
		curve.generateBasePoint();
		
		// load private and public keys
		String privatePath = "res/key/key.pri";
		String publicPath = "res/key/key.pub";
		privateKey = "";
		publicKey = "";
		try {
			privateKey = new String(Files.readAllBytes(Paths.get(privatePath)));
			publicKey = new String(Files.readAllBytes(Paths.get(publicPath)));
		} catch (NoSuchFileException e) {
			System.out.println("No keys defined yet.");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// load ECDSA
		ECDSAImplementation.ECDSAInit();
		ECDSAImplementation.debugECDSAProperties();
		
		// load inbox emails
		inboxMails = EmailApp.checkMail(EmailApp.INBOX);
		
		// load outbox emails
		outboxMails = EmailApp.checkMail(EmailApp.OUTBOX);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Main.getInstance();
	}

}

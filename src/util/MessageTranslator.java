package util;

/**
 * Class for translating ciphertext to plaintext and otherwise
 */

/**
 * @author nashir
 *
 */
public class MessageTranslator {

	public static int MODE_ENCRYPTION = 0;
	public static int MODE_DECRYPTION = 1;

	public static int MODE_ECB = 0;
	public static int MODE_CBC = 1;
	public static int MODE_CFB8 = 2;
	
	private static int[] PTable = new int[]{
		53,62, 7, 4,53,16,21,42,11,25,59,48,
		25, 1,15,51, 3,32, 4,29,38,43,13,24,
		44,12,35,41,60,18,61,17,16, 6,63,39,
		56,33,49, 1,26,18,45,20,34,52,37,27,
		64,10,22,33, 8,42,36,30, 2, 9,28,14,
		29, 4,11,40,19,36,54,17,14, 3,46,57,
		 9,58,54,26, 7,50,19,30,41,10,31,38,
		27,37,47,13,64,20, 5, 5,55,23,15,61
	};
	
	private static byte[][] S1 = new byte[][]{
		{14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7},
		{ 0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8},
		{ 4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0},
	    {15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13}
	};
	
	private static byte[][] S2 = new byte[][]{
		{15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10},
		{ 3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5},
		{ 0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15},
	    {13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9}
	};
	
	private static String IV;
	private static String[] keys;
	
	private static int mode1;
	private static int mode2;
	
	public static void setIV(String _IV) {
		IV = _IV;
	}
	
	public static void setMode1(int mode) {
		mode1 = mode;
	}
	
	public static void setMode2(int mode) {
		mode2 = mode;
	}
	
	public static void prepareEncryptKeys(String initialKey) {
		keys = new String[16];
		int[] shiftTable = new int[]{1,2,1,3,2,1,2,2,1,1,2,3,1,1,1,2};
		
		boolean showSteps = false;
		int div = 16;
		byte[][] result = new byte[div][div];
		int counter = 1;
		boolean shiftDir = true; //true = shift left, false = shift right
		
		if (showSteps) System.out.println("Step 1 Key initialization");
		System.out.println("Initial Key: " + initialKey);
			
		byte[] currentKey = KeyGenerator.getBytes(initialKey, div);
		
		do{
			if (showSteps){
				System.out.println();
				System.out.println("Step 2.1 Chop current key into N(0)..N(15)");
				KeyGenerator.printBytes(currentKey);
				KeyGenerator.printBytesToInt(currentKey);
			
				System.out.println();
				System.out.println("Step 2.2 Substitute MSB and LSB of N(0)..N(15) with 1 and 0");
				System.out.println("and Step 3.1 Generate relatively primes for N(0)..N(15)");
				
			}
			
			
			
			byte[][] m = new byte[div][div];
			for (int i = 0; i < currentKey.length; i++) {
				byte b = KeyGenerator.substituteBits(currentKey[i]);
				byte[] temp = KeyGenerator.generateRelativePrimes(KeyGenerator.byteToPositiveInteger(b), 16, true, false); //harus pastiin jadi positif int dulu, untuk kasus b > 127
				m[i] = temp;
				
				if (showSteps){
					System.out.println("Before substitute: " + KeyGenerator.byteToBits(currentKey[i]) + " (" + KeyGenerator.byteToPositiveInteger(currentKey[i]) +")");
					System.out.println("After substitute: " + KeyGenerator.byteToBits(b) + " (" + KeyGenerator.byteToPositiveInteger(b) +")");
					System.out.print(div + " relatively primes of " + KeyGenerator.byteToPositiveInteger(b) + ": ");
					KeyGenerator.printBytesToInt(temp);
					System.out.println();
				}
			}
			
			
			byte[] generatedKey = KeyGenerator.doXORMatrix(m, div);
			
			if (showSteps){
				System.out.println();
				System.out.println("Step 3.2 Make 16x16 Matrix");
				KeyGenerator.printMatrix(m);
				
				System.out.println();
				System.out.println("Step 4 Bitwise XOR result");
				KeyGenerator.printBytes(generatedKey);
				KeyGenerator.printBytesToInt(generatedKey);
				
				System.out.println();
				System.out.println("Step 5 Generated: Internal key "+counter);
				KeyGenerator.printBytes(generatedKey);
				KeyGenerator.printBytesToInt(generatedKey);
			}
			

			//shifting
			for(int i = 0; i < generatedKey.length; i++){
				//validation for shiftTable length and number of repetition?
				//alternatative: generatedKey[i] = wrappedShift(generatedKey[i], dir, shiftTable[counter-1]);
				generatedKey[i] = KeyGenerator.wrappedShift(generatedKey[i], shiftDir, shiftTable[i]);
				shiftDir = !shiftDir; //swap shift direction every byte
			}
			
			if (showSteps){
				System.out.println("[AFTER SHIFT] Step 5 Generated: Final Internal key "+counter);
				KeyGenerator.printBytes(generatedKey);
				KeyGenerator.printBytesToInt(generatedKey);
			}
			
			
			currentKey = generatedKey;
			result[counter-1] = generatedKey;
			counter++;
			shiftDir = true;
		} while(counter<=16);
		
		System.out.println();
		System.out.println("The 16 Generated keys:");
		for (int i = 0; i < result.length; i++) {
			System.out.print("Internal key " + String.format("%2d", i+1) + ": ");
			KeyGenerator.printBytes(result[i]);
			keys[i] = KeyGenerator.printBytes2(result[i]);
		}
	}
	
	public static void prepareDecryptKeys(String initialKey) {
		prepareEncryptKeys(initialKey);
		
		if (mode2 != MODE_CFB8) {
			String[] temp = new String[keys.length];
			for (int i = 0; i < keys.length; i++) {
				temp[i] = keys[keys.length-i-1];
			}
			for (int i = 0; i < keys.length; i++) {
				keys[i] = temp[i];
			}
		}
	}
	
	/**
	 * translate the whole message
	 * @param message
	 * @param key
	 * @return message's whole translation
	 */
	public static byte[] translateMessage(byte[] message) {
		int i = 0;
		while ((message.length + i) % 16 != 0) {
			i++;
		}
		
		byte[] message2 = new byte[message.length + i];
		for (int j = 0; j < message2.length; j++) {
			if (j < message.length) {
				message2[j] = message[j];
			} else {
				message2[j] = 0;
			}
		}
		
		if (mode2 == MODE_CFB8) {
			return translateMessageCFB8(message);
		}
		
		byte[] result = new byte[message2.length];
		
		i = 0;
		byte[] temp = new byte[16];
		while (i < message2.length) {
			for (int j = 0; j < 16; j++) {
				temp[j] = message2[i+j];
			}
			
			if (mode1 == MODE_ENCRYPTION && mode2 == MODE_CBC) {
				if (i == 0) {
					for (int j = 0; j < 16; j++) {
						temp[j] ^= (byte) IV.charAt(j);
					}
				} else {
					for (int j = 0; j < 16; j++) {
						temp[j] ^= result[i+j-16];
					}
				}
			}
			
			byte[] block = translateBlock(temp);
			
			if (mode1 == MODE_DECRYPTION && mode2 == MODE_CBC) {
				if (i == 0) {
					for (int j = 0; j < 16; j++) {
						block[j] ^= (byte) IV.charAt(j);
					}
				} else {
					for (int j = 0; j < 16; j++) {
						block[j] ^= message2[i+j-16];
					}
				}
			}
			
			for (int j = 0; j < 16; j++) {
				result[i+j] = block[j];
			}
			
			i += 16;
		}
		
		return result;
	}
	
	private static byte[] translateMessageCFB8(byte[] message) {
		byte[] feedback = new byte[16];
		for (int i = 0; i < 16; i++) {
			feedback[i] = (byte) IV.charAt(i);
		}
		
		int m = 0;
		byte[] result = new byte[message.length];
		while (m < message.length) {
			byte[] block = translateBlock(feedback);
			result[m] = (byte) (block[0] ^ message[m]);

			for (int i = 0; i < 15; i++) {
				feedback[i] = block[i+1];
			}
			if (mode1 == MODE_ENCRYPTION) {
				feedback[15] = result[m];
			} else {
				feedback[15] = message[m];
			}
			
			m++;
		}
		
		return result;
	}
	
	/**
	 * translate a message block of 128 bits (16 bytes)
	 * @param block
	 * @param key
	 * @return block's translation
	 */
	private static byte[] translateBlock(byte[] block) {
		byte[] L = new byte[8];
		byte[] R = new byte[8];
		
		for (int i = 0; i < 8; i++) {
			L[i] = block[i];
			R[i] = block[8+i];
		}
		
		for (int i = 0; i < 16; i++) {
			roundFunction(L, R, i);
		}
		
		byte[] result = new byte[16];
		for (int i = 0; i < 8; i++) {
			result[i] = R[i];
			result[i+8] = L[i];
		}
		
		return result;
	}
	
	private static void roundFunction(byte[] left, byte[] right, int iteration) {
		byte[] temp = new byte[8];
		for (int i = 0; i < 8; i++) {
			temp[i] = left[i];
		}
		
		for (int i = 0; i < 8; i++) {
			left[i] = right[i];
		}
		
		String key = keys[iteration];
		
		StringBuilder sb = new StringBuilder(key);
		for (int i = 127; i > 0; i -= 4) {
			sb.deleteCharAt(i);
		}
		key = sb.toString();
		
		String r = "";
		for (int i = 0; i < 8; i++) {
			r += KeyGenerator.byteToBits(right[i]);
		}
		
		String r2 = "";
		for (int i = 0; i < PTable.length; i++) {
			r2 += r.charAt(PTable[i]-1);
		}
		
		String c = xorString(key, r2);
		int i = 0;
		String result = "";
		while (i < c.length()) {
			result += getSBox(c.substring(i, i+6), i/6);
			i += 6;
		}
		
		for (int j = 0; j < 8; j++) {
			right[j] = (byte) (temp[j] ^ binaryToByte(result.substring(6*j, 6*j+8)));
		}
	}
	
	public static String xorString(String b0, String b1) {
		String result = "";
		for (int i = 0; i < b0.length(); i++) {
			if (b0.charAt(i) == b1.charAt(i)) {
				result += "0";
			} else {
				result += "1";
			}
		}
		return result;
	}
	
	private static String getSBox(String input, int box) {
		String row = "";
		row += input.charAt(0);
		row += input.charAt(input.length()-1);
		
		String col = input.substring(1, input.length()-1);
		
		switch (box) {
		case 0:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 1:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 2:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 3:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 4:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 5:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 6:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 7:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 8:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 9:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 10:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 11:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 12:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 13:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		case 14:
			return KeyGenerator.byteToBits(S1[binaryToPosByte(row)][binaryToPosByte(col)]);
		default:
			return KeyGenerator.byteToBits(S2[binaryToPosByte(row)][binaryToPosByte(col)]);
		}
	}
	
	public static byte binaryToByte(String binary) {
		byte result = 0;
		for (int i = 1; i < binary.length(); i++) {
			result *= 2;
			if (binary.charAt(i) == '1') {
				result += 1;
			}
		}
		if (binary.charAt(0) == '1') {
			result -= Math.pow(2, 7);
		}
		return result;
	}
	
	public static int binaryToPosByte(String binary) {
		int result = 0;
		for (int i = 1; i < binary.length(); i++) {
			result *= 2;
			if (binary.charAt(i) == '1') {
				result += 1;
			}
		}
		return result;
	}
}

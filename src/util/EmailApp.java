package util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;

import config.GlobalConfiguration;

public class EmailApp {

	public static String INBOX = "INBOX";
	public static String OUTBOX = "[Gmail]/Sent Mail";

	//private static boolean textIsHtml = false;
	
	public static Email[] checkMail(String folder) {
		Email[] retval = null;
		Properties props = new Properties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
			Session session = Session.getInstance(props, null);
			Store store = session.getStore();
			store.connect("imap.gmail.com", GlobalConfiguration.username, GlobalConfiguration.password);
			Folder inbox = store.getFolder(folder);
			inbox.open(Folder.READ_ONLY);
			Message[] msgs = inbox.getMessages();

			retval = new Email[msgs.length];
			for (int i = 0; i < msgs.length; i++) {
				//Message msg = msgs[msgs.length - 1];
				try {
					if (msgs[i].getContent() instanceof Multipart) {
						Multipart mp = (Multipart) msgs[i].getContent();
						ArrayList<String> temp = new ArrayList<String>();
						for (int j = 0; j < mp.getCount(); j++) {
							BodyPart bp = mp.getBodyPart(j);
							if (!Part.ATTACHMENT.equalsIgnoreCase(bp.getDisposition()) && StringUtils.isBlank(bp.getFileName())) {
								continue; // dealing with attachments only
							}
							temp.add(bp.getFileName());
							//TODO uncomment
							/*InputStream is = bp.getInputStream();
							String s = "res/inbox/attachments/";
							if (OUTBOX.equals(folder)) s = "res/outbox/attachments/";
							File f = new File(s + bp.getFileName());
							FileOutputStream fos = new FileOutputStream(f);
							byte[] buf = new byte[4096];
							int bytesRead;
							while ((bytesRead = is.read(buf)) != -1) {
								fos.write(buf, 0, bytesRead);
							}
							fos.close();*/
						}
						String[] bps = temp.toArray(new String[temp.size()]);
						retval[i] = new Email(msgs[i].getFrom(), msgs[i].getSentDate(), msgs[i].getSubject(), writePart(msgs[i]), bps);
					} else if (msgs[i].getContent() instanceof String) {
						retval[i] = new Email(msgs[i].getFrom(), msgs[i].getSentDate(), msgs[i].getSubject(), writePart(msgs[i]), new String[0]);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			// close folder and store (normally in a finally block)
			inbox.close(false);
			store.close();
		} catch (Exception mex) {
			mex.printStackTrace();
		}
		
		return retval;
	}
	
	/**
	 * Return the primary text content of the message.
	 */
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String) p.getContent();
			//textIsHtml = p.isMimeType("text/html");
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			// prefer html text over plain text
			Multipart mp = (Multipart) p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
				Part bp = mp.getBodyPart(i);
				if (bp.isMimeType("text/plain")) {
					if (text == null)
						text = getText(bp);
					continue;
				} else if (bp.isMimeType("text/html")) {
					String s = getText(bp);
					if (s != null)
						return s;
				} else {
					return getText(bp);
				}
			}
			return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}

		return null;
	}
	
	public static void sendMail(String subject, String teks, String email_to) {
		// Assuming you are sending email through relay.jangosmtp.net
		String host = GlobalConfiguration.mail_host;
		final String username = GlobalConfiguration.username;
		final String password = GlobalConfiguration.password;
		String from = GlobalConfiguration.from;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");

		// Get the Session object.
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email_to));

			// Set Subject: header field
			message.setSubject(subject);

			MimeBodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText(teks);

			//MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			//messageBodyPart2.setText(sign);

			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart1);
			//multipart.addBodyPart(messageBodyPart2);

			// 6) set the multiplart object to the message object
			message.setContent(multipart, "text/html");

			// Send message
			Transport.send(message);

			//System.out.println("Sent message successfully....");
			JOptionPane.showMessageDialog(null, "Message sent successfully.");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Email[] fetch(String folder) {
		Email[] retval = null;
		try {
			// create properties field
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", GlobalConfiguration.pop3_host);
			properties.put("mail.pop3.port", "995");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);
			// emailSession.setDebug(true);
			
			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("pop3s");
			
			store.connect(GlobalConfiguration.pop3_host, GlobalConfiguration.username, GlobalConfiguration.password);
			
			// create the folder object and open it
			Folder emailFolder = store.getFolder(folder);
			emailFolder.open(Folder.HOLDS_MESSAGES);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			//System.out.println("messages.length---" + messages.length);
			
			//seharusnya messages.length
			retval = new Email[messages.length];
			for (int i = 0; i < messages.length; i++) {
				//Message message = messages[messages.length - 1];
				//System.out.println("---------------------------------");
				//writePart(messages[i]);
				//messages[i].writeTo(System.out);
				retval[i] = new Email(messages[i].getFrom(), messages[i].getSentDate(), messages[i].getSubject(), writePart(messages[i]));
			}
			
			// close the store and folder objects
			emailFolder.close(false);
			store.close();
		} catch (NoSuchProviderException e) {
				e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retval;
	}
	
	public static String writePart(Part p) throws Exception {
		String retval = "";
		if (p instanceof Message)
			//Call methos writeEnvelope
			writeEnvelope((Message) p);

		//System.out.println("----------------------------");
		//System.out.println("CONTENT-TYPE: " + p.getContentType());

		//check if the content is plain text
		if (p.isMimeType("text/plain")) {
			//System.out.println("This is plain text");
			//System.out.println("---------------------------");
			//System.out.println((String) p.getContent());
			retval += (String) p.getContent();
		} 
		//check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			//System.out.println("This is a Multipart");
			//System.out.println("---------------------------");
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++)
				retval += writePart(mp.getBodyPart(i));
		} 
		// check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {
			//System.out.println("This is a Nested Message");
			//System.out.println("---------------------------");
			retval += writePart((Part) p.getContent());
		}
		// check if the content is an inline image
		else if (p.isMimeType("image/jpeg")) {
			//System.out.println("--------> image/jpeg");
			Object o = p.getContent();

			InputStream x = (InputStream) o;
			// Construct the required byte array
			//System.out.println("x.length = " + x.available());
			int i = 0;
			byte[] bArray = new byte[x.available()];

			while ((i = (int) ((InputStream) x).available()) > 0) {
				int result = (int) (((InputStream) x).read(bArray));
				if (result == -1)
					break;
			}
			FileOutputStream f2 = new FileOutputStream("res/image/image.jpg");
			f2.write(bArray);
		} else if (p.getContentType().contains("image/")) {
			//System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p
					.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				//System.out.println("This is a string");
				//System.out.println("---------------------------");
				//System.out.println((String) o);
			} else if (o instanceof InputStream) {
				//System.out.println("This is just an input stream");
				//System.out.println("---------------------------");
				InputStream is = (InputStream) o;
				is = (InputStream) o;
				int c;
				while ((c = is.read()) != -1)
					System.out.write(c);
			} else {
				//System.out.println("This is an unknown type");
				//System.out.println("---------------------------");
				//System.out.println(o.toString());
			}
		}
		return retval;
	}
	
	public static String writeEnvelope(Message m) throws Exception {
		String retval = "";
		//System.out.println("This is the message envelope");
		//System.out.println("---------------------------");
		Address[] a;

		// FROM
		retval += "From\t: ";
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				//System.out.println("FROM: " + a[j].toString());
				retval += a[j].toString() + "; ";
		}
		retval += "\n";

		// TO
		retval += "To\t: ";
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				//System.out.println("TO: " + a[j].toString());
				retval += a[j].toString() + "; ";
		}
		retval += "\n";

		// SUBJECT
		if (m.getSubject() != null)
			//System.out.println("SUBJECT: " + m.getSubject());
			retval += "Subject\t: " + m.getSubject() + "\n";
		
		return retval;
	}
}

package util;

import javax.swing.table.AbstractTableModel;

public class EmailTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 6852801490544468986L;
	
	String[] columns = {"From", "Subject", "Timestamp"};
	Object[][] data = {};
	
	public EmailTableModel(Email[] mails) {
		data = new Object[mails.length][5];
		for (int i = 0; i < mails.length; i++) {
			data[i] = mails[i].getInboxData();
		}
	}
	
	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return data.length;
	}

	@Override
	public String getColumnName(int col) {
		return columns[col];
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

}

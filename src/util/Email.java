package util;

import java.util.Date;

import javax.mail.Address;

public class Email {

	private String[] to;
	private Address[] from;
	private String subject;
	private String content;
	private String[] attachments;
	private boolean isEncrypted;
	private boolean isSigned;
	private String sign;
	private Date timestamp;
	
	public Email(Address[] from, Date date, String subject, String content) {
		this.from = new Address[from.length];
		for (int i = 0; i < from.length; i++) this.from[i] = from[i];
		
		timestamp = date;
		this.subject = subject;
		this.content = content;
		
		attachments = null;
	}
	
	public Email(Address[] from, Date date, String subject, String content, String[] attachments) {
		this.from = new Address[from.length];
		for (int i = 0; i < from.length; i++) this.from[i] = from[i];
		
		timestamp = date;
		this.subject = subject;
		this.content = content;
		
		this.attachments = new String[attachments.length];
		for (int i = 0; i < attachments.length; i++) this.attachments[i] = attachments[i];
	}
	
	public Object[] getInboxData() {
		Object[] retval = {from[0], subject, timestamp, isEncrypted, isSigned};
		return retval;
	}
	
	public String[] getAttachments() {
		return attachments;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String toString() {
		String retval = "From\t: " + from[0] + "\n";
		retval += "Subject\t: " + subject + "\n";
		retval += "Date\t: " + timestamp + "\n";
		retval += "======================================================================================================\n\n";
		retval += content;
		return retval;
	}
}

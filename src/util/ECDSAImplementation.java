package util;

import java.io.UnsupportedEncodingException; 
import java.math.BigInteger;
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException; 
import java.security.SecureRandom;
import java.util.Random;

public class ECDSAImplementation {
    /**** P-192 ****/
    public static BigInteger p = new BigInteger ("ffffffff00000001000000000000000000000000ffffffffffffffffffffffff",16);
	public static BigInteger a = new BigInteger ("ffffffff00000001000000000000000000000000fffffffffffffffffffffffc",16);
	public static BigInteger b = new BigInteger ("5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b",16);
	public static ECDSAPoint G = new ECDSAPoint(new BigInteger ("6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296",16),new BigInteger ("4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5",16));
	public static BigInteger n = new BigInteger ("ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551",16);
	public static BigInteger private_key = new BigInteger("100600789377147023461072082702934359437205647979148520028102921837690663199091");
	public static ECDSAPoint public_key = new ECDSAPoint(new BigInteger("19212080424464390540063596887575911368054913997721170672481358068768598080413"), new BigInteger("12323678239184652402150218964501452920351633338132841133745710599253251301001"));
	//public static BigInteger private_key;
	//public static ECDSAPoint public_key;
	private static BigInteger zero = BigInteger.ZERO;
	
	public static BigInteger randomBigInteger(BigInteger n) {
        Random rnd = new Random();
        int maxNumBitLength = n.bitLength();
        BigInteger aRandomBigInt;
        do {
            aRandomBigInt = new BigInteger(maxNumBitLength, rnd);
            // compare random number lessthan ginven number
        } while (aRandomBigInt.compareTo(n) > 0); 
        return aRandomBigInt;
    }
	
	private static ECDSAPoint penjumlahanTitik(ECDSAPoint p1, ECDSAPoint p2){
		ECDSAPoint p3 = new ECDSAPoint();
	    BigInteger slope,x,y, temp;
	    if(p1.Y.equals(zero) || p2.Y.equals(zero) || (p1.infinity && p2.infinity) || (p1.X.equals(p2.X) && p1.Y.equals(p2.Y.negate()))){
	    	p3.infinity = true;
	    } else {  // IF POINTS ARE EQUAL
	    	if ((p1.X.equals(p2.X)) &&  (p1.Y.equals(p2.Y))) {
	    		temp = new BigInteger(""+3);
	    		slope = ((temp.multiply(p1.X.pow(2))).add(a)).multiply((p1.Y.add(p1.Y)).modInverse(p));
	    		x = slope.multiply(slope).subtract(p1.X.add(p1.X));  //(slope*slope) - (2*p1.x);
	    		x = x.mod(p);
	    		y = slope.multiply(p1.X.subtract(x)).subtract(p1.Y);
	    	}else{	// POINTS ARE NOT EQUAL		
	    		if (p1.infinity) {
	    			x = p2.X;
	    			y = p2.Y;
	    		} else if (p2.infinity) {
	    			x = p1.X;
	    			y = p1.Y;
	    		} else{
	    			temp = (p2.X.subtract(p1.X)).modInverse(p);
	    			slope = (p2.Y.subtract(p1.Y)).multiply(temp);  		//(p2.y - p1.y)/(p2.x - p1.x);
	    			x = ((slope.multiply(slope)).subtract(p1.X)).subtract(p2.X);	
	    			//(slope*slope) - p1.x - p2.x;
	    			y = (slope.multiply(p1.X.subtract(x))).subtract(p1.Y);			//slope*(p1.x - x) - p1.y;
	    		}				
	    		
	    		x = x.mod(p);
	    	}	
	    	y = y.mod(p);
	    	p3 = new ECDSAPoint(x,y);
	    }
	    return p3;
	   }

	private static ECDSAPoint perkalianTitik(BigInteger s, ECDSAPoint p){
		String binS = s.toString(2);
		ECDSAPoint q = new ECDSAPoint();
		q.infinity = true;
		
		if (binS.substring(0,1).equals("1")){
			q = p;
		}
		
		for (int i = 1; i< binS.length(); i++){
			q = penjumlahanTitik(q,q);
			if (binS.substring(i, i+1).equals("1")){
				q = penjumlahanTitik(q,p);
			}
		}
		return q;
	}
	
	public static void ECDSAInit(){
		SecureRandom sr = new SecureRandom();
		private_key = new BigInteger(512,sr);  
		private_key = private_key.mod((n.subtract(new BigInteger("1")))); 
		public_key = perkalianTitik(private_key,G);
	}
	
	private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    }
		
	public static void debugECDSAProperties(){
		System.out.println("P : "+p);
		System.out.println("a : "+a);
		System.out.println("b : "+b);
		System.out.println("G: "+G.toString());
		System.out.println("n : "+n);
		System.out.println("Q : "+private_key+ " (private_key)");
		System.out.println("Kunci Publik: "+public_key.toString());
	}
	
	public static ECDSAPoint writeSign(BigInteger h){
		//udah bener
		BigInteger kinv, r, s, k;
		
		/* Generate nilai-k terlebih dahulu 
		SecureRandom sr = new SecureRandom();
		k = new BigInteger(512, sr);  
		k = k.mod(n.subtract(new BigInteger("1")));
		while((k.compareTo(java.math.BigInteger.ZERO) == 0) || !(k.gcd(n).compareTo(java.math.BigInteger.ONE) == 0)){
	  		k = new BigInteger(512,sr); 
	  		k = k.mod(n.subtract(new BigInteger("1")));
		} */
		k = new BigInteger("40121827141716901269858813858503838757905054601140215498543171027954052047503");
		System.out.println("K : "+k);
		ECDSAPoint Kp = perkalianTitik(k, G); //k * G
		
		if (Kp.X.equals(zero)){
			return writeSign(h);
		}
		
		r = Kp.X.mod(n); //x1 mod N
		kinv = k.modInverse(n);  //k-1
		s = (kinv.multiply((h.add((private_key.multiply(r)))))).mod(n);
		
		if (s.compareTo(java.math.BigInteger.ZERO) == 0) return writeSign(h);
		
		return new ECDSAPoint(r,s);
	}
	
	public static boolean verifySign(ECDSAPoint sign, BigInteger h){
		BigInteger w, u1, u2,v;
		BigInteger r = sign.X;
		BigInteger s = sign.Y;
		ECDSAPoint pt;
		
		System.out.println("Verify: "+sign.toString());
		if (r.compareTo(n)==1 || r.compareTo(BigInteger.ONE)==-1 || s.compareTo(n)==1 || s.compareTo(BigInteger.ONE)==-1){
			System.out.println("Diluar range");
			return false;
		}
		
		w = s.modInverse(n);
		u1 = h.multiply(w).mod(n);
		u2 = r.multiply(w).mod(n);
		
		ECDSAPoint u1P = perkalianTitik(u1, G);
		ECDSAPoint u2Q = perkalianTitik(u2, public_key);
		
		pt = penjumlahanTitik(u1P, u2Q);
		v = pt.X;
		
		System.out.println("v: "+v);
		System.out.println("r: "+r);
		if(v.compareTo(r.mod(n))==0)
			return true;
		else
			return false;
	}
	
	public static byte[] SHA1(String text)  { 
		try {    
		//Ini hanya SHA-1
			MessageDigest md;
		    md = MessageDigest.getInstance("SHA-1");
		    byte[] sha1hash = new byte[40];
		    md.update(text.getBytes("iso-8859-1"), 0, text.length());
		    sha1hash = md.digest();
		    //return convertToHex(sha1hash);	
		    return sha1hash;
		} catch (Exception e){e.printStackTrace(); return null;}
	}
}

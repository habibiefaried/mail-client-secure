package util;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * 
 * @author rivai
 *
 */

public class KeyGenerator {

	private static String chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public static String generateIV() {
		String result = "";
		Random r = new Random();
		for (int i = 0; i < 16; i++) {
			result += chars.charAt(r.nextInt(chars.length()));
		}
		System.out.println("IV: " + result);
		return result;
	}
	
	//Euclid's method to find GCD of two numbers
	public static long GCD(long i1, long i2){
		//base
		if (i2 == 0){
			return i1;
		}
		//recurrent
		return GCD(i2, i1%i2);
	}
	
	//check if two numbers are relative prime to each other (AKA check whether the GCD of the two number is 1)
	public static boolean isRelativePrime(long i1, long i2){
		return GCD(i1, i2) == 1;
	}
	
	//generate n relative prime numbers of num
	//assumes num is a positive integer > 1
	//generate from the greatest number (i-1) if backward == true, generate from 1 and forward otherwise
	//set unique = true to return a list of unique numbers, otherwise to return n numbers no matter what
	public static byte[] generateRelativePrimes(int num, int n, boolean backward, boolean unique){
		List<Integer> l = new ArrayList<Integer>();
		
		if (backward){
			for (int i = num-1; i > 0; i--){
				if (isRelativePrime(num, i)) l.add(i);
				if (l.size() >= n) break;
			}
		} else {
			for (int i = 1; i < num; i++){
				if (isRelativePrime(num, i)) l.add(i);
				if (l.size() >= n) break;
			}
		}
		
		//if not unique, copy last element of l until its size == n
		if (l.size() < n && !unique){
			while(l.size() < n){
				l.add(l.get(l.size() - 1));
			}
		}
		
		return listToByteArray(l);
	}
	
	//Convert binary string into byte array, each element of the array represents a positive integer 0-255
//	public static byte[] getBytes(String s, int byteLength){
//		byte[] ret = new byte[byteLength];
//		
//		for (int i = 0; i < byteLength; i++){
//			String temp = s.substring(9*i,9*i+8);
//			//System.out.println("DEBUG: getBytes " + Integer.parseInt(temp,2));
//			ret[i] = (byte) (Integer.parseInt(temp,2));
//			//ret[i] =  (byte) (0x000000FF & ret[i]);
//			//System.out.println("DEBUG: getBytes " + (0x000000FF & ret[i]));
//			//System.out.println("DEBUG: getBytes " + ret[i]);
//		}
//		
//		return ret;
//	}
	
	//convert a string into byte array, each char in the string will be converted into its byte representation
	public static byte[] getBytes(String s, int byteLength){
		byte[] ret = new byte[byteLength];
		
		for (int i = 0; i < byteLength; i++){
			char temp = s.charAt(i);
			ret[i] = (byte) temp;
		}
		
		return ret;
	}
	
	//convert int (32bit) into byte (8bit)
	public static byte toByte(int i){
		return (byte)((i & 0x000000FF) );
	}
	
	//Convert List of Integer into byte[]
	public static byte[] listToByteArray(List<Integer> l){
		byte[] ret = new byte[l.size()];
		for(int i = 0; i < ret.length; i++){
			ret[i] = toByte(l.get(i));
		}
		return ret;
	}
	
	//print positive decimal value of each byte in matrix m
	public static void printMatrix(byte[][] m){
		for (byte[] ls : m) {
			for (byte l : ls) {
				System.out.printf("%4d", byteToPositiveInteger(l));
			}
			System.out.println();
		}
	}
	
	//xor each element within the same diagonal in m
	public static byte[] doXORMatrix(byte[][] m, int numToGenerate){
		byte[] ret = new byte[numToGenerate];
		
		for (int i = 0; i < numToGenerate; i++){
			for (int j = 0; j < numToGenerate; j++){
				ret[i] ^= m[j][(j+i)%numToGenerate];
			}
		}
		
		return ret;
	}
	
	//shift and wrap bits to the left
	public static byte wrappedShiftLeft(byte b, int numShift){
		return (byte) ((b << numShift) | ((b&0x000000FF) >>> (8-numShift)));
	}
	
	//shift and wrap bits to the right
	public static byte wrappedShiftRight(byte b, int numShift){
		return (byte) (((b&0x000000FF) >>> numShift) | (b << (8-numShift)));
	}
	
	//combination of the two method above in a single call
	public static byte wrappedShift(byte b, boolean left, int numShift){
		if (left)
			return wrappedShiftLeft(b, numShift);
		else //shift right
			return wrappedShiftRight(b, numShift);
	}
	
	//substitute MSB of b with 1, and LSB of which with 0
	public static byte substituteBits(byte b){
		return (byte) ((b&0x000000FE)|0x00000080);// | (b&0x000000FA));
	}
	
	
	//print positive decimal value of each element in data array
	public static void printBytesToInt(byte[] data){
		for (int i=0; i<data.length; i++) {
			System.out.print(byteToPositiveInteger(data[i])+" ");
	    }
	    System.out.println();
	}
	
	//return positive decimal value of b
	public static int byteToPositiveInteger(byte b){
		StringBuffer buf = new StringBuffer();
	      for (int i=0; i<8; i++)
	         buf.append((int)(b>>(8-(i+1)) & 0x0001));
	      return Integer.parseInt(buf.toString(),2);
	}
	
	
	//print each byte in data array as binary string
	public static void printBytes(byte[] data) {
	      for (int i=0; i<data.length; i++) {
	         System.out.print(byteToBits(data[i])+" ");
	      }
	      System.out.println();
	}
	
	public static String printBytes2(byte[] data) {
		String result = "";
		for (int i = 0; i < data.length; i++) {
			result += byteToBits(data[i]);
		}
		return result;
	}
	
	//convert b into its binary string form
	public static String byteToBits(byte b) {
	      StringBuffer buf = new StringBuffer();
	      for (int i=0; i<8; i++)
	         buf.append((int)(b>>(8-(i+1)) & 0x0001));
	      return buf.toString();
	}

	
	   
	
	//Debugging method
	public static void d(String arg){
		System.out.println("DEBUG: " + m() + " " + arg);
	}
	
	//get calling method name
	public static String m(){
		return Thread.currentThread().getStackTrace()[3].getMethodName();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] shiftTable = new int[]{1,2,1,3,2,1,2,2,1,1,2,3,1,1,1,2};
		
		boolean showSteps = false;
		int div = 16;
		byte[][] result = new byte[div][div];
		int counter = 1;
		boolean shiftDir = true; //true = shift left, false = shift right
		String initialKey;
		
		if (args.length == 1)//initial key
			initialKey = args[0];
		else { //no initial key argument given, prompt user to input it via stdin
			Scanner scanner = new Scanner(System.in);
			do{
				System.out.println("Enter initial key (16-byte/char): ");
				initialKey = scanner.nextLine();
			} while (initialKey.length() != 16);
			scanner.close();
		}
		if (showSteps) System.out.println("Step 1 Key initialization");
		System.out.println("Initial Key: " + initialKey);
			
		byte[] currentKey = getBytes(initialKey, div);
		
		do{
			if (showSteps){
				System.out.println();
				System.out.println("Step 2.1 Chop current key into N(0)..N(15)");
				printBytes(currentKey);
				printBytesToInt(currentKey);
			
				System.out.println();
				System.out.println("Step 2.2 Substitute MSB and LSB of N(0)..N(15) with 1 and 0");
				System.out.println("and Step 3.1 Generate relatively primes for N(0)..N(15)");
				
			}
			
			
			
			byte[][] m = new byte[div][div];
			for (int i = 0; i < currentKey.length; i++) {
				byte b = substituteBits(currentKey[i]);
				byte[] temp = generateRelativePrimes(byteToPositiveInteger(b), 16, true, false); //harus pastiin jadi positif int dulu, untuk kasus b > 127
				m[i] = temp;
				
				if (showSteps){
					System.out.println("Before substitute: " + byteToBits(currentKey[i]) + " (" + byteToPositiveInteger(currentKey[i]) +")");
					System.out.println("After substitute: " + byteToBits(b) + " (" + byteToPositiveInteger(b) +")");
					System.out.print(div + " relatively primes of " + byteToPositiveInteger(b) + ": ");
					printBytesToInt(temp);
					System.out.println();
				}
			}
			
			
			byte[] generatedKey = doXORMatrix(m, div);
			
			if (showSteps){
				System.out.println();
				System.out.println("Step 3.2 Make 16x16 Matrix");
				printMatrix(m);
				
				System.out.println();
				System.out.println("Step 4 Bitwise XOR result");
				printBytes(generatedKey);
				printBytesToInt(generatedKey);
				
				System.out.println();
				System.out.println("Step 5 Generated: Internal key "+counter);
				printBytes(generatedKey);
				printBytesToInt(generatedKey);
			}
			

			//shifting
			for(int i = 0; i < generatedKey.length; i++){
				//validation for shiftTable length and number of repetition?
				//alternatative: generatedKey[i] = wrappedShift(generatedKey[i], dir, shiftTable[counter-1]);
				generatedKey[i] = wrappedShift(generatedKey[i], shiftDir, shiftTable[i]);
				shiftDir = !shiftDir; //swap shift direction every byte
			}
			
			if (showSteps){
				System.out.println("[AFTER SHIFT] Step 5 Generated: Final Internal key "+counter);
				printBytes(generatedKey);
				printBytesToInt(generatedKey);
			}
			
			
			currentKey = generatedKey;
			result[counter-1] = generatedKey;
			counter++;
			shiftDir = true;
		} while(counter<=16); 
		
		System.out.println();
		System.out.println("The 16 Generated keys:");
		for (int i = 0; i < result.length; i++) {
			System.out.print("Internal key " + String.format("%2d", i+1) + ": ");
			printBytes(result[i]);
		}
		
	}

}

package util;

import java.math.BigInteger;

public class ECDSAPoint {
	public BigInteger X = new BigInteger("0");
	public BigInteger Y = new BigInteger("0");
	public boolean infinity = false;
		
	public ECDSAPoint(){
		X = new BigInteger("0");
		Y = new BigInteger("0");
	}
	
	public ECDSAPoint(String _x, String _y){
		X = new BigInteger(_x,16);
		Y = new BigInteger(_y,16);
	}
	
	public ECDSAPoint(BigInteger _x, BigInteger _y){
		X = _x;
		Y = _y;
	}
	
	public String toString(){
		String xHex = X.toString(16); 
		String yHex = Y.toString(16);
		return xHex+"-"+yHex;
	}
	
	public void printHex(){
		String xHex = X.toString(16); 
		String yHex = Y.toString(16);
		System.out.println(xHex+" "+yHex);
		BigInteger normalX = new BigInteger(xHex,16);
		BigInteger normalY = new BigInteger(yHex,16);
		System.out.println(normalX+" "+normalY);
	}
}

import config.GlobalConfiguration;
import util.ECDSAImplementation;
import util.ECDSAPoint;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Properties;
import java.util.Scanner;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class MainApp {
	private static BigInteger e_content;
	private static ECDSAPoint e_hash;
	
	
	public static void sendMail(String teks, String sign, String email_to){
		// Assuming you are sending email through relay.jangosmtp.net
	      String host = GlobalConfiguration.mail_host;
	      final String username = GlobalConfiguration.username;
	      final String password = GlobalConfiguration.password;
	      String from = GlobalConfiguration.from;
	      
	      Properties props = new Properties();
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      props.put("mail.smtp.host", host);
	      props.put("mail.smtp.port", "587");

	      // Get the Session object.
	      Session session = Session.getInstance(props,
	      new javax.mail.Authenticator() {
	         protected PasswordAuthentication getPasswordAuthentication() {
	            return new PasswordAuthentication(username, password);
	         }
	      });

	      try {
	         // Create a default MimeMessage object.
	         Message message = new MimeMessage(session);

	         // Set From: header field of the header.
	         message.setFrom(new InternetAddress(from));

	         // Set To: header field of the header.
	         message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(email_to));

	         // Set Subject: header field
	         message.setSubject("Testing Subject");

	         MimeBodyPart messageBodyPart1 = new MimeBodyPart();  
	         messageBodyPart1.setText(teks);  
	         
	         MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
	         messageBodyPart2.setText(sign);  
	         
	         Multipart multipart = new MimeMultipart();  
	         multipart.addBodyPart(messageBodyPart1);  
	         multipart.addBodyPart(messageBodyPart2);  
	       
	         //6) set the multiplart object to the message object  
	         message.setContent(multipart,"text/html");
	         
	         // Send message
	         Transport.send(message);

	         System.out.println("Sent message successfully....");

	      } catch (MessagingException e) {
	            throw new RuntimeException(e);
	      }
	}
	
	public static void main(String[] args) {
		try {
			
			//checkMail();
			//System.out.println(ECDSAImplementation.SHA1("Habibie"));
			String ujicoba = "Sample Letter About Resolved Issue Dear Ms. McDonald, I am writing to follow up on our discussion last Wednesday. As you requested, I filed a work order with the facilities department regarding the damage to the ascending escalator you reported. An examination of the escalator found that there was a shoelace caught in the gears. Said shoelace has since been removed, and the escalator was tested extensively to ensure this would not happen again. I am pleased to report that the tests were passed with flying colors, and the escalator has been reopened. Thank you for bringing this issue to our attention. Please continue to inform us should you have any other problems in our facilities. Sincerely Marilyn Novak Property Manager Novak Skyscraper Construction";
			//System.out.println();
			
			BigInteger hash = new BigInteger(ECDSAImplementation.SHA1(ujicoba)); //misalkan ini hashplain
			ECDSAImplementation.ECDSAInit();
			ECDSAImplementation.debugECDSAProperties();
			
			
			ECDSAPoint EP = ECDSAImplementation.writeSign(hash); //pengambilan dsa
			EP.printHex();
			System.out.println("Signature: "+EP.toString());			
			System.out.println(ECDSAImplementation.verifySign(EP, new BigInteger(ECDSAImplementation.SHA1(ujicoba))));
			sendMail(ujicoba,EP.toString(),"testingonly2016@gmail.com");
			checkMail();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void parseBodyPart(Multipart mp){
		//next, balikan wajib string
		int i = 0;
		System.out.println("========================================");
		System.out.println("               CONTENT                  ");
		System.out.println("========================================");
		try {
			for (i = 0; i<mp.getCount(); i++){
				BodyPart bp = mp.getBodyPart(i);
				String disposition = bp.getDisposition();
				System.out.println("Content no-"+i+":");
				if (disposition != null && (disposition.equalsIgnoreCase("ATTACHMENT"))) {
					System.out.println("Ada attachment di email");
					DataHandler handler = bp.getDataHandler();
		            System.out.println("file name : " + handler.getName());
				} else {
					Object bc = bp.getContent();
					if (bc instanceof Multipart) parseBodyPart((Multipart)bc);
					else {
						String str = bp.getContent().toString();
						if (i == 0) {
							e_content = new BigInteger(ECDSAImplementation.SHA1(str));
						}
						else if (i == 1) {
							String[] parts = str.split("-");
							e_hash = new ECDSAPoint(parts[0],parts[1]);
						}
						System.out.println(str);
					}
				}
				
			}
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void checkMail() {
	    Properties props = new Properties();
	    props.setProperty("mail.store.protocol", "imaps");
	    try {
	        Session session = Session.getInstance(props, null);
	        Store store = session.getStore();
	        store.connect("imap.gmail.com", GlobalConfiguration.username, GlobalConfiguration.password);
	        Folder inbox = store.getFolder("INBOX");
	        inbox.open(Folder.READ_ONLY);
	        Message[] msgs = inbox.getMessages();
	        
	        for (Message msg : msgs) {
	            //Message msg = msgs[msgs.length-1];
	        	try {
	                Address[] in = msg.getFrom();
	                for (Address address : in) {
	                    System.out.println("FROM:" + address.toString());
	                }
	                Multipart mp = (Multipart) msg.getContent();
	                
	                
	                System.out.println("SENT DATE:" + msg.getSentDate());
	                System.out.println("SUBJECT:" + msg.getSubject());
	                parseBodyPart(mp); //
	                
	                System.out.println(ECDSAImplementation.verifySign(e_hash, e_content)); //coba verifikasi */
	                
	                System.out.println("Press any key then enter to open next message...");
	                System.out.println("Or, write 'QUIT' to quit app");

	                Scanner sc = new Scanner(System.in);
	                String S = sc.nextLine();
	                if (S.equals("QUIT")) break;
	                
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	        // close folder and store (normally in a finally block)
	        inbox.close(false);
	        store.close();

	    } catch (Exception mex) {
	        mex.printStackTrace();
	    }
	}
}
